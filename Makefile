BUILD_DIR=build

CMAKE_DEBUG_FLAGS=-DENABLE_DEBUG=ON -DENABLE_TESTS=OFF -DENABLE_CPPCHECK=OFF -DENABLE_SANITIZERS=ON
CMAKE_TEST_FLAGS=${CMAKE_DEBUG_FLAGS} -DENABLE_COVERAGE=ON
MAKE_FLAGS=-j$(shell nproc)

CMAKE_CHECK_FLAGS=-DENABLE_DEBUG=ON -DENABLE_TESTS=OFF -DENABLE_CPPCHECK=ON -DENABLE_SANITIZERS=ON

USER_PROTO_SRC_DIR:=user/proto
WORKER_PROTO_SRC_DIR:=worker/proto
COMMON_RPC_PROTO_SRC_DIR:=common/rpc/proto
COMMON_CONFIG_PROTO_SRC_DIR:=common/config/proto

EXAMPLES_DIR:=examples
CONFIG_DIR:=examples/config
SNAPSHOTS_DIR:=${BUILD_DIR}/snapshots

debug:
	cmake -B ${BUILD_DIR} ${CMAKE_DEBUG_FLAGS}
	make ${MAKE_FLAGS} -C ${BUILD_DIR} all
	cp -r ${EXAMPLES_DIR} ${BUILD_DIR}
	[ ! -d ${SNAPSHOTS_DIR} ] && mkdir -p ${SNAPSHOTS_DIR} || :

build: clean proto user-proto
	cmake -B ${BUILD_DIR}
	make ${MAKE_FLAGS} -C ${BUILD_DIR} all

check: clean
	cmake -B ${BUILD_DIR} ${CMAKE_CHECK_FLAGS}
	scan-build make ${MAKE_FLAGS} -C ${BUILD_DIR} all

test:
	cmake -B ${BUILD_DIR} ${CMAKE_TEST_FLAGS}
	scan-build make ${MAKE_FLAGS} -C ${BUILD_DIR} all test

clean:
	rm -rf ${BUILD_DIR}

docker:
	DOCKER_BUILDKIT=1 docker build -t abeille:latest .

# ---------------------------------- PROTO ---------------------------------- #

proto: user-proto worker-proto common-rpc-proto common-config-proto

user-proto:
	protoc -I ${USER_PROTO_SRC_DIR} --cpp_out=${USER_PROTO_SRC_DIR}  ${USER_PROTO_SRC_DIR}/*.proto

worker-proto:
	protoc -I ${WORKER_PROTO_SRC_DIR} --cpp_out=${WORKER_PROTO_SRC_DIR}  ${WORKER_PROTO_SRC_DIR}/*.proto

common-rpc-proto:
	protoc -I ${COMMON_RPC_PROTO_SRC_DIR} --cpp_out=${COMMON_RPC_PROTO_SRC_DIR}  ${COMMON_RPC_PROTO_SRC_DIR}/*.proto
	protoc -I ${COMMON_RPC_PROTO_SRC_DIR} --grpc_out=${COMMON_RPC_PROTO_SRC_DIR} --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` ${COMMON_RPC_PROTO_SRC_DIR}/abeille.proto

common-config-proto:
	protoc -I ${COMMON_CONFIG_PROTO_SRC_DIR} --cpp_out=${COMMON_CONFIG_PROTO_SRC_DIR}  ${COMMON_CONFIG_PROTO_SRC_DIR}/*.proto

# ---------------------------------- RUNNERS ---------------------------------- #

run-raft-node%:
	./build/bin/abeille-raft ${CONFIG_DIR}/raft_config_$*.json

run-user-client:
	./build/bin/user_client ${CONFIG_DIR}/user_config.json ${CONFIG_DIR}/client_config.json

run-worker-client:
	./build/bin/worker_client ${CONFIG_DIR}/worker_config.json ${CONFIG_DIR}/client_config.json

