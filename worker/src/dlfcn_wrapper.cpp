#include "worker/include/dlfcn_wrapper.hpp"

namespace abeille {
namespace worker {

DLFCNWrapper::DLFCNWrapper(const std::string &filepath, int mode) noexcept
    : handle_(dlopen(filepath.c_str(), mode)) {}

DLFCNWrapper::~DLFCNWrapper() noexcept {
  if (handle_ != nullptr) {
    dlclose(handle_);
  }
}

bool DLFCNWrapper::ok() const noexcept {
  return handle_ != nullptr;
}

void *DLFCNWrapper::GetAddress(const std::string &symbol) const noexcept {
  return dlsym(handle_, symbol.c_str());
}

}  // namespace worker
}  // namespace abeille
