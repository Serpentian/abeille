#include "worker/include/registry.hpp"

#include <google/protobuf/util/json_util.h>

#include <fstream>

using google::protobuf::util::JsonStringToMessage;

#include "common/config/proto/config.pb.h"
#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"

namespace abeille {
namespace worker {

error Registry::Init(const std::string &filepath) noexcept {
  std::string data;
  error err = GetFileData(filepath, data);
  if (!err.ok()) {
    return err;
  }

  WorkerConfig worker_config;
  auto status = JsonStringToMessage(data, &worker_config);
  if (!status.ok()) {
    return error(status.message().as_string());
  }

  SetLibsDirectory(worker_config.libs_dir());

  return error();
}

void Registry::SetLibsDirectory(const std::string &dirpath) noexcept {
  libs_dir_path_ = dirpath;
  std::filesystem::create_directory(dirpath);
}

error Registry::SaveLibrary(uint64_t user_id, const Bytes &bytes) {
  std::string filepath = GetFilepath(user_id);
  std::ofstream file(filepath, std::ios::trunc);
  if (!file.good()) {
    return error("failed to open file for output");
  }
  file << bytes;
  return error();
}

std::string Registry::GetFilepath(uint64_t user_id) const noexcept {
  return libs_dir_path_ / ("libtask-" + std::to_string(user_id) + ".so");
}

}  // namespace worker
}  // namespace abeille
