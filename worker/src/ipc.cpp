#include "worker/include/ipc.hpp"

#include <thread>

#include "common/rpc/proto/abeille.pb.h"
#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"
#include "worker/include/abstract_task.hpp"
#include "worker/include/dlfcn_wrapper.hpp"

namespace abeille {
namespace worker {

IPC::IPC(std::shared_ptr<Registry> registry) noexcept : registry_(registry) {}

IPC::~IPC() noexcept {
  if (!shutdown_) {
    Shutdown();
  }
}

error IPC::Run() {
  error err = stream_.CreatePipes();
  if (!err.ok()) {
    return err;
  }

  err = mayfly_stream_.CreatePipes();
  if (!err.ok()) {
    return err;
  }

  pid_t pid = fork();
  if (pid == -1) {
    return error("failed to fork");
  } else if (pid == 0) {
    CloseReadChannel(stream_.resp);
    CloseWriteChannel(stream_.req);

    CloseReadChannel(mayfly_stream_.resp);
    CloseWriteChannel(mayfly_stream_.req);

    err = runManagerJob();
    if (!err.ok()) {
      LOG_ERROR(err);
    }

    CloseReadChannel(stream_.req);
    CloseWriteChannel(stream_.resp);

    CloseReadChannel(mayfly_stream_.req);
    CloseWriteChannel(mayfly_stream_.resp);

    exit(0);
  }

  CloseReadChannel(stream_.req);
  CloseWriteChannel(stream_.resp);

  CloseReadChannel(mayfly_stream_.req);
  CloseWriteChannel(mayfly_stream_.resp);

  return error();
}

void IPC::Shutdown() noexcept {
  shutdown_ = true;
  stream_.ClosePipes();
  mayfly_stream_.ClosePipes();
}

error IPC::ProcessTask(const IPCRequest &request, Bytes &response) {
  // send command to spawn a mayfly
  error err =
      WritePODToChannel(GetWriteChannel(stream_.req), IPCCommand::CreateMayfly);
  if (!err.ok()) {
    return err;
  }

  // write to the mayfly
  std::string message = request.SerializeAsString();
  err = WriteToChannel(GetWriteChannel(mayfly_stream_.req), message);
  if (!err.ok()) {
    return err;
  }

  // read the response from the mayfly
  err = ReadFromChannel(GetReadChannel(mayfly_stream_.resp), response);
  if (!err.ok()) {
    return err;
  }

  return error();
}

error IPC::runManagerJob() {
  IPCCommand command;
  int read_channel = GetReadChannel(stream_.req);
  while (!shutdown_ && ReadPODFromChannel(read_channel, command).ok()) {
    if (command == IPCCommand::CreateMayfly) {
      error err = createMayfly();
      if (!err.ok()) {
        return err;
      }
    }
  }
  return error();
}

error IPC::createMayfly() {
  pid_t pid = fork();
  if (pid == -1) {
    return error("failed to fork");
  } else if (pid == 0) {
    CloseReadChannel(stream_.resp);
    CloseWriteChannel(stream_.req);

    CloseReadChannel(mayfly_stream_.resp);
    CloseWriteChannel(mayfly_stream_.req);

    error err = runMayflyJob();
    if (!err.ok()) {
      LOG_ERROR(err);
    }

    CloseReadChannel(stream_.req);
    CloseWriteChannel(stream_.resp);

    CloseReadChannel(mayfly_stream_.req);
    CloseWriteChannel(mayfly_stream_.resp);

    exit(0);
  }
  return error();
}

error IPC::runMayflyJob() {
  std::string message;
  error err = ReadFromChannel(GetReadChannel(mayfly_stream_.req), message);
  if (!err.ok()) {
    return err;
  }

  IPCRequest req;
  req.ParseFromString(message);

  Bytes task_result;
  err = processTaskData(req.user_id(), req.data(), task_result);
  if (!err.ok()) {
    return err;
  }

  err = WriteToChannel(GetWriteChannel(mayfly_stream_.resp), task_result);
  if (!err.ok()) {
    return err;
  }

  return error();
}

error IPC::processTaskData(uint64_t user_id, const Bytes &task_data,
                           Bytes &task_result) const {
  std::string filepath = registry_->GetFilepath(user_id);

  DLFCNWrapper dw(filepath, RTLD_LAZY);
  if (!dw.ok()) {
    return error("failed to get the handle");
  }

  auto func_ptr = dw.GetAddress(ProcessAbstractTaskDataFuncName);
  if (!func_ptr) {
    return error("failed to find the symbol");
  }

  auto func = reinterpret_cast<decltype(&ProcessAbstractTaskData)>(func_ptr);
  error err = func(task_data, task_result);
  if (!err.ok()) {
    return err;
  }

  LOG_INFO("successfully processed the task");

  return error();
}

}  // namespace worker
}  // namespace abeille
