#include <chrono>
#include <csignal>
#include <memory>
#include <string>
#include <thread>

#include "common/utils/include/convert.hpp"
#include "worker/include/ipc.hpp"
#include "worker/include/worker_client.hpp"

using abeille::worker::Client;
using abeille::worker::IPC;
using abeille::worker::Registry;

bool shutdown = false;
std::shared_ptr<Registry> registry = nullptr;
std::shared_ptr<IPC> ipc = nullptr;
std::shared_ptr<Client> client = nullptr;

void onShutdown() {
  client->Shutdown();
}

void signalHandler(int signal) noexcept {
  shutdown = true;
}

void listenShutdown() noexcept {
  while (!shutdown) {
    std::this_thread::sleep_for(std::chrono::seconds(2));
  }
  onShutdown();
}

error setup(char *argv[]) noexcept {
  std::string worker_config_path = argv[1];
  registry = std::make_shared<Registry>();
  error err = registry->Init(worker_config_path);
  if (!err.ok()) {
    return err;
  }

  ipc = std::make_shared<IPC>(registry);
  err = ipc->Run();
  if (!err.ok()) {
    return err;
  }

  std::string client_config_path = argv[2];
  client = std::make_shared<Client>(registry, ipc);
  err = client->Init(client_config_path);
  if (!err.ok()) {
    return err;
  }

  return error();
}

void run() noexcept {
  std::thread([&] { client->Run(); }).detach();
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    std::cout << "expected [worker config] and [client config]" << std::endl;
    return -1;
  }

  error err = setup(argv);
  if (!err.ok()) {
    std::cout << err.what() << std::endl;
    return -1;
  }

  run();

  std::signal(SIGINT, signalHandler);
  std::thread(listenShutdown).join();

  return 0;
}
