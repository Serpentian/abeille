#ifndef ABEILLE_WORKER_ABSTRACT_TASK_HPP_
#define ABEILLE_WORKER_ABSTRACT_TASK_HPP_

#include "common/errors/include/errors.hpp"
#include "common/utils/include/types.hpp"

namespace abeille {
namespace worker {

static constexpr const char ProcessAbstractTaskDataFuncName[] =
    "ProcessAbstractTaskData";

// Wrap the function with extern "C" to have unmangled symbol.
extern "C" {
error ProcessAbstractTaskData(const Bytes &task_data, Bytes &task_result);
}

}  // namespace worker
}  // namespace abeille

#endif  // ABEILLE_WORKER_ABSTRACT_TASK_HPP_
