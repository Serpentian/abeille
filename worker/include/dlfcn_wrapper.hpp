#ifndef ABEILLE_WORKER_DLFCN_WRAPPER_HPP_
#define ABEILLE_WORKER_DLFCN_WRAPPER_HPP_

#include <string>

#include "dlfcn.h"

namespace abeille {
namespace worker {

class DLFCNWrapper {
 public:
  DLFCNWrapper(const std::string &filepath, int mode) noexcept;
  ~DLFCNWrapper() noexcept;

  bool ok() const noexcept;
  void *GetAddress(const std::string &symbol) const noexcept;

 private:
  void *handle_;
};

}  // namespace worker
}  // namespace abeille

#endif  // ABEILLE_WORKER_DLFCN_WRAPPER_HPP_
