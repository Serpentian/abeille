#ifndef ABEILLE_WORKER_CLIENT_HPP_
#define ABEILLE_WORKER_CLIENT_HPP_

#include <grpcpp/grpcpp.h>

#include <memory>
#include <queue>
#include <thread>

#include "common/errors/include/errors.hpp"
#include "common/rpc/include/client.hpp"
#include "common/rpc/proto/abeille.grpc.pb.h"
#include "common/utils/include/logger.hpp"
#include "common/utils/include/types.hpp"
#include "worker/include/ipc.hpp"
#include "worker/include/registry.hpp"

using grpc::Channel;
using grpc::ClientContext;
using namespace std::placeholders;

namespace abeille {
namespace worker {

using ConnReq = WorkerConnectRequest;
using ConnResp = WorkerConnectResponse;
using WorkerClient = abeille::rpc::Client<ConnReq, ConnResp, WorkerService>;

class Client : public WorkerClient {
 public:
  Client(std::shared_ptr<Registry> registry, std::shared_ptr<IPC> ipc) noexcept;

  void CommandHandler(const ConnResp &resp) override;
  void StatusHandler(ConnReq &req) override;

 private:
  error handleCommandAssign(const ConnResp &resp);
  error handleCommandProcess(const ConnResp &resp);
  error handleCommandRedirect(const ConnResp &resp);

  error handleStatusCompleted(ConnReq &req);

  void processTaskData(const TaskID &task_id, const Bytes &task_data);

 private:
  uint64_t leader_id_ = 0;
  std::queue<TaskState> task_states_;
  WorkerStatus status_ = WORKER_STATUS_IDLE;

  std::shared_ptr<Registry> registry_ = nullptr;
  std::shared_ptr<IPC> ipc_ = nullptr;
};

}  // namespace worker
}  // namespace abeille

#endif  // ABEILLE_WORKER_CLIENT_HPP_
