#ifndef ABEILLE_RAFT_TASK_MANAGER_HPP_
#define ABEILLE_RAFT_TASK_MANAGER_HPP_

#include <memory>
#include <queue>
#include <thread>

#include "common/errors/include/errors.hpp"
#include "common/rpc/proto/abeille.grpc.pb.h"
#include "common/utils/include/types.hpp"
#include "raft/core/include/core.hpp"

namespace abeille {
namespace raft {

// forward declaration
class Core;

class TaskManager {
 public:
  using CoreRef = Core &;

  TaskManager() noexcept = delete;
  explicit TaskManager(CoreRef core) : core_(core) {}
  ~TaskManager() = default;

  error UploadTaskData(const Bytes &task_data, const TaskID &task_id,
                       const Bytes &lib);
  error ProcessTask(const TaskWrapper &task_wrapper);

  error UploadTaskResult(const TaskID &task_id, const Bytes &task_result);
  error SendTaskResult(const TaskWrapper &task_wrapper);

 private:
  CoreRef core_;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_TASK_MANAGER_HPP_
