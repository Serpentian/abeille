#ifndef ABEILLE_RAFT_H_
#define ABEILLE_RAFT_H_

#include <getopt.h>

#include <csignal>
#include <fstream>
#include <memory>
#include <thread>

// For protobuf version checking:
#include "common/errors/include/errors.hpp"
#include "common/rpc/proto/abeille.pb.h"
#include "common/utils/include/logger.hpp"
#include "common/utils/include/types.hpp"
#include "raft/core/include/config.hpp"
#include "raft/core/include/core.hpp"

using namespace abeille::raft;

namespace abeille {

static bool shutdown = false;
static std::unique_ptr<Core> core_ptr = nullptr;
static Config &config = Config::GetInstance();

void signal_handler(int signal) {
  shutdown = true;
}

void listen_shutdown() {
  while (true) {
    if (shutdown) {
      if (core_ptr) {
        core_ptr->Shutdown();
      }
      break;
    }

    std::this_thread::sleep_for(std::chrono::seconds(2));
  }
}

void help() {
  printf("Usage: abeille-raft [OPTIONS] [-i] FILE\n");
  printf("Launch abeille-raft node\n\n");
  printf(
      "Mandatory arguments to long options are mandatory "
      "for short options too.\n");
  printf("  -c, --config=FILE     launch with config FILE\n");
  printf("  -s, --snapshot=FILE   enable snapshoting\n");
  printf("  -a, --after=N         snapshot after N entries\n");
  printf("  -h, --help            display this help and exit\n\n");
}

error parse_command_line_arguments(int argc, char **argv) {
  error failure = error(error::Code::FAILURE);
  std::string config_name, snapshot_to;
  Index after = 20;
  int c;

  // will be overwritten if find config after -i
  if (argc != 1) {
    config_name = argv[argc - 1];
  }

  // while(true) is according to getopt docs
  while (true) {
    static struct option long_options[] = {
        {"snapshot", required_argument, 0, 's'},
        {"after", required_argument, 0, 'a'},
        {"config", required_argument, 0, 'c'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}};

    int option_index = 0;
    c = getopt_long(argc, argv, "s:a:c:h", long_options, &option_index);

    if (c == -1) {
      break;
    }

    switch (c) {
      case 's':
        snapshot_to = optarg;
        break;

      case 'c':
        config_name = optarg;
        break;

      case 'a':
        after = std::stoull(optarg);
        break;

      case 'h':
        help();
        return failure;

      default:
        return failure;
    }
  }

  if (after < 2) {
    LOG_ERROR("Number of entries must be more than 1");
    return failure;
  }

  config.SetConfigName(config_name);
  config.SetSnapshotAfter(after);
  config.SetSnapshotTo(snapshot_to);

  return error();
}

error run() {
  // Core initializing and running
  LOG_INFO("Initializing abeille-raft...");
  core_ptr = std::make_unique<Core>();
  if (!core_ptr->Init().ok()) {
    LOG_ERROR("Core wasn't initialized. Exiting...");
    return error(error::Code::FAILURE);
  }

  LOG_INFO("Launching abeille-raft ...");
  core_ptr->Run();
  return error();
}

}  // namespace abeille

#endif  // ABEILLE_RAFT_H_
