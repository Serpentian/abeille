#include "raft/cmd/include/abeille_raft.hpp"
using namespace abeille;

int main(int argc, char *argv[]) {
  // Verify that the version of the library that we linked against is
  // compatible with the version of the headers we compiled against
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  // Launch abeille-raft
  if (!parse_command_line_arguments(argc, argv).ok()) {
    return EXIT_FAILURE;
  }

  if (!run().ok()) {
    return EXIT_FAILURE;
  }

  // Shutdown it
  std::signal(SIGINT, signal_handler);
  auto shutdown_thread = std::thread(&listen_shutdown);
  shutdown_thread.join();
  google::protobuf::ShutdownProtobufLibrary();

  return EXIT_SUCCESS;
}
