#ifndef ABEILLE_RAFT_LOG_H_
#define ABEILLE_RAFT_LOG_H_

#include <deque>
#include <mutex>
#include <string>

#include "common/errors/include/errors.hpp"
#include "common/rpc/proto/abeille.grpc.pb.h"
#include "common/utils/include/types.hpp"
#include "raft/core/include/config.hpp"

namespace abeille {
namespace raft {

// Can be changed from several threads
// Indexing starts with 1 !!!
class Log {
 public:
  typedef const Entry &EntryConstReference;
  typedef Entry &EntryReference;
  typedef Entry *EntryPointer;
  typedef error Status;

  Log() noexcept = default;
  ~Log() noexcept = default;

  Index LastIndex() const noexcept;
  Index StartIndex() const noexcept;
  // May throw exception if index == 0 || index > LastIndex()
  size_t Size() const noexcept;
  EntryConstReference operator[](Index pos) const;

  // These one needs the guard. Segv otherwise
  bool HasEntryWith(Index idx, Term term = 0) noexcept;

  // Modifiers. Lock_guard is mandatory here
  size_t DropAfter(Index index) noexcept;
  void Append(EntryConstReference entry) noexcept;
  size_t Discard() noexcept;
  size_t DropBefore(Index idx) noexcept;
  void SetStartIndex(Index idx) noexcept;

 private:
  // fast index acess & ability to pop from back and front
  std::deque<Entry> log_;
  Index start_index_ = 1;
  std::mutex mutex_;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_LOG_H_
