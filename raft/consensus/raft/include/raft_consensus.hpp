#ifndef ABEILLE_RAFT_RAFT_H_
#define ABEILLE_RAFT_RAFT_H_

#include <chrono>
#include <condition_variable>
#include <memory>
#include <thread>

#include "common/errors/include/errors.hpp"
#include "common/rpc/proto/abeille.pb.h"
#include "common/utils/include/types.hpp"
#include "raft/consensus/log/include/log.hpp"
#include "raft/consensus/raft_pool/include/peer.hpp"
#include "raft/consensus/raft_pool/include/raft_pool.hpp"
#include "raft/consensus/state_machine/include/state_machine.hpp"
#include "raft/core/include/core.hpp"

namespace abeille {
namespace raft {

// forward declaration
class Peer;
class Core;
class StateMachine;

class RaftConsensus {
 public:
  enum class State { FOLLOWER, CANDIDATE, LEADER };
  typedef Core &CoreRef;
  typedef std::shared_ptr<Log> LogPtr;
  typedef std::unique_ptr<RaftPool> RaftThreadPoolPtr;
  typedef std::unique_ptr<StateMachine> StateMachinePtr;
  typedef std::chrono::steady_clock::time_point TimePoint;
  typedef std::chrono::milliseconds TimeDuration;
  typedef std::chrono::steady_clock Clock;
  typedef std::atomic<uint64_t> ThreadsNum;

  RaftConsensus() noexcept = delete;
  explicit RaftConsensus(CoreRef core) noexcept;
  ~RaftConsensus();

  // Starts/shutdown timerThreadMain and
  void Run() noexcept;
  void Shutdown() noexcept;

  // Pretty self-explanatory
  bool IsLeader() const noexcept {
    return id_ == leader_id_;
  }
  ServerId GetLeaderID() const noexcept {
    return leader_id_;
  }
  Term GetTerm() const noexcept {
    return current_term_;
  }

  // Interface for appending Entry to cluster
  void AppendEntry(const Entry &entry) noexcept;

  // process RPCs from another server
  void HandleAppendEntry(const AppendEntryRequest *msg,
                         AppendEntryResponse *resp);
  void HandleRequestVote(const RequestVoteRequest *msg,
                         RequestVoteResponse *resp);

  void HandleInstallSnapshot(const InstallSnapshotRequest *msg,
                             InstallSnapshotResponse *resp);

 private:
  // Main logic for every node depending on current state of server
  void peerThreadMain(std::shared_ptr<Peer> peer);

  // For election process:
  void timerThreadMain();
  void startNewElection();
  void resetElectionTimer();
  void becomeLeader();

  // Update term, convert to follower,
  void stepDown(uint64_t term);

  // Entries commiting
  void advanceCommitIndex();
  void commitEntry(Index index);

  // RPC request
  void appendEntry(std::unique_lock<std::mutex>& lck, Peer &peer);
  void requestVote(std::unique_lock<std::mutex>& lck, Peer &peer);
  void installSnapshot(std::unique_lock<std::mutex>& lck, Peer &peer);

  Term getLastLogTerm() noexcept;

 private:
  uint64_t id_ = 0;
  uint64_t leader_id_ = 0;
  State state_ = State::FOLLOWER;
  Index voted_for_ = 0;
  bool shutdown_ = false;
  Term current_term_ = 0;

  CoreRef core_;
  LogPtr log_ = nullptr;
  StateMachinePtr state_machine_;
  RaftThreadPoolPtr raft_pool_ = nullptr;

  // Triggers when almost anything inside raft happens
  std::mutex mutex_;
  std::condition_variable raft_state_changed_;
  ThreadsNum num_peers_threads_;

  TimeDuration ELECTION_TIMEOUT_ = (std::chrono::milliseconds(150));
  // when the next heartbeat should be sent
  TimeDuration HEARTBEAT_PERIOD_ =
      std::chrono::milliseconds(ELECTION_TIMEOUT_.count() / 3);
  // No RPC request for the peer after failure response
  TimeDuration FAILURE_PROROGUE_ =
      std::chrono::milliseconds(ELECTION_TIMEOUT_.count() / 2);

  TimeDuration RPC_DEADLINE_ =
      std::chrono::milliseconds(ELECTION_TIMEOUT_.count() / 5);

  // the time at which timerThreadMain() should start a new election
  TimePoint start_new_election_at_ = TimePoint::max();
  // don't respond to request votes until
  TimePoint hold_election_for = TimePoint::min();

  // this thread executes timer_thread_main
  std::thread timer_thread_;

  friend class RaftPool;
  friend class Peer;
  // friend class StateMachine;
};

}  // namespace raft
}  // namespace abeille

#endif  //  ABEILLE_RAFT_RAFT_H_
