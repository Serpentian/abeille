#include "raft/consensus/raft_pool/include/raft_pool.hpp"

#include <grpcpp/grpcpp.h>

#include <algorithm>
#include <cmath>

#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"
#include "common/utils/include/types.hpp"

namespace abeille {
namespace raft {

RaftPool::RaftPool(RaftPool::RaftRef raft) noexcept : raft_(raft) {
  Config &config = Config::GetInstance();
  auto peers_adresses = config.GetPeers();
  for (const auto &addr : peers_adresses) {
    if (!addr.empty()) {
      ServerId temp_id = address2uint(addr);
      peers_.push_back(std::make_shared<Peer>(
          grpc::CreateChannel(addr, grpc::InsecureChannelCredentials()), raft_,
          temp_id));
    }
  }
}

void RaftPool::Run() noexcept {
  std::for_each(peers_.begin(), peers_.end(),
                [](PeerRef &peer) { peer->Run(peer); });
}

void RaftPool::BeginRequestVote() noexcept {
  std::for_each(peers_.begin(), peers_.end(),
                [](PeerRef &peer) { peer->BeginRequestVote(); });
}

bool RaftPool::MajorityVotes() const noexcept {
  long long votes_num = std::count_if(
      peers_.cbegin(), peers_.cend(),
      [](const RaftPool::PeerRef &peer) { return peer->HaveVote(); });

  bool vote_yourself = raft_.voted_for_ == raft_.id_;
  return votes_num + vote_yourself >= std::llround((peers_.size() + 1) / 2.0);
}

Index RaftPool::PoolCommitIndex(Index &prev_commit_idx) noexcept {
  bool stored_majority = true;
  Index temp_index = prev_commit_idx;

  while (stored_majority) {
    ++temp_index;
    stored_majority =
        static_cast<long long>(std::count_if(peers_.cbegin(), peers_.cend(),
                                             [&temp_index](const PeerRef peer) {
                                               return peer->GetMatchIndex() >=
                                                      temp_index;
                                             }) +
                               1) >= std::llround((peers_.size() + 1) / 2.0);
  }

  return --temp_index;
}

void RaftPool::UpdatePeers() noexcept {
  std::for_each(peers_.begin(), peers_.end(),
                [](PeerRef &peer) { peer->UpdatePeer(); });
}

void RaftPool::Shutdown() noexcept {
  std::for_each(peers_.begin(), peers_.end(),
                [](PeerRef &peer) { peer->Shutdown(); });
}

}  // namespace raft
}  // namespace abeille
