#include "raft/consensus/raft_pool/include/peer.hpp"

#include "common/rpc/proto/abeille.pb.h"
#include "common/utils/include/logger.hpp"
#include "raft/consensus/raft/include/raft_consensus.hpp"

namespace abeille {
namespace raft {

Peer::Peer(std::shared_ptr<grpc::Channel> channel, Peer::RaftRef raft,
           uint64_t id) noexcept
    : id_(id),
      raft_(raft),
      stub_(RaftService::NewStub(channel)),
      next_index_(raft_.log_->LastIndex() + 1) {}

void Peer::Run(std::shared_ptr<Peer> self) noexcept {
  LOG_INFO("Starting peer thread for [%s]", uint2address(id_).c_str());
  std::thread(&RaftConsensus::peerThreadMain, &raft_, self).detach();
}

void Peer::BeginRequestVote() noexcept {
  have_vote_ = false;
  vote_request_done_ = false;
}

void Peer::UpdatePeer() noexcept {
  match_index_ = 0;
  next_index_ = raft_.log_->LastIndex() + 1;
}

}  // namespace raft
}  // namespace abeille
