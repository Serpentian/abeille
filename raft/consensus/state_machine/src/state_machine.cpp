#include "raft/consensus/state_machine/include/state_machine.hpp"

#include <signal.h>
#include <unistd.h>

#include <chrono>

#include "common/utils/include/logger.hpp"

namespace abeille {
namespace raft {

// Index must be checked before invoking this method!
// These entries cannot be snapshoted before commiting
void StateMachine::Commit(Index index, bool is_leader) noexcept {
  std::lock_guard<std::mutex> guard(mutex_);

  for (Index idx = commit_index_ + 1; idx <= index; ++idx) {
    auto status = applyCommand(log_[idx], is_leader);
    if (!status.ok()) {
      LOG_ERROR("Error while applying commited entry");
    }
  }

  LOG_INFO("Updating commit index from %lu to %lu", commit_index_, index);
  commit_index_ = index;
  commit_term_ = log_[index].term();

  state_changed_.notify_all();
}

// no internal lock!
error StateMachine::applyCommand(Log::EntryConstReference entry,
                                 bool is_leader) noexcept {

  auto tw = TaskIDWrapper(entry.task_wrapper().task_id());
  // auto task_pair = std::make_pair(tw, entry.task_wrapper());
  auto failure = error(error::Code::FAILURE);

  if (entry.command() == RaftCommand::RAFT_COMMAND_ADD) {
    auto add_task_status = entry.add_request().to();
    if (add_task_status == TaskStatus::TASK_STATUS_ASSIGNED) {
      assigned_.insert_or_assign(tw, entry.task_wrapper());
      if (is_leader) {
        error err = mgr_.ProcessTask(entry.task_wrapper());
        if (!err.ok()) {
          LOG_ERROR(err);
          return failure;
        }
      }
    } else if (add_task_status == TaskStatus::TASK_STATUS_COMPLETED) {
      completed_.insert_or_assign(tw, entry.task_wrapper());
    } else {
      LOG_ERROR("Unknown add_to task status");
      return failure;
    }

  } else if (entry.command() == RaftCommand::RAFT_COMMAND_MOVE) {
    auto move_to_task_status = entry.move_request().to();
    auto move_from_task_status = entry.move_request().from();

    if (move_from_task_status == TaskStatus::TASK_STATUS_ASSIGNED) {
      assigned_.erase(tw);
    } else if (move_from_task_status == TaskStatus::TASK_STATUS_COMPLETED) {
      LOG_WARN("Deleting completed task from state machine");
      completed_.erase(tw);
    } else {
      LOG_ERROR("Unknown move_from task status");
      return failure;
    }

    if (move_to_task_status == TaskStatus::TASK_STATUS_ASSIGNED) {
      assigned_.insert_or_assign(tw, entry.task_wrapper());
      if (is_leader) {
        error err = mgr_.ProcessTask(entry.task_wrapper());
        if (!err.ok()) {
          LOG_ERROR(err);
          return failure;
        }
      }
    } else if (move_to_task_status == TaskStatus::TASK_STATUS_COMPLETED) {
      completed_.insert_or_assign(tw, entry.task_wrapper());
      if (is_leader) {
        error err = mgr_.SendTaskResult(entry.task_wrapper());
        if (!err.ok()) {
          LOG_ERROR(err);
          return failure;
        }
      }
    } else {
      LOG_ERROR("Unknown move_to task status");
      return failure;
    }
  } else {
    LOG_ERROR("Unknown Entry's command");
    return failure;
  }

  return error();
}

// ----------------- Snapshotting logic ----------------- //

void StateMachine::Run() noexcept {
  /*
  try {
    ipc_ = std::make_unique<SnapshotIPC>();
  } catch (const std::system_error& err) {
    LOG_ERROR(err.what());
    return;
  }
  */

  snapshot_thread_ = std::thread(&StateMachine::snapshotThreadMain, this);
}

void StateMachine::Shutdown() noexcept {
  if (!exiting_) {
    exiting_ = true;
    state_changed_.notify_all();
    if (snapshot_thread_.joinable()) {
      snapshot_thread_.join();
    }

    /*
    if (fork_snapshot_thread_.joinable()) {
      fork_snapshot_thread_.join();
    }
    */

    LOG_INFO("State machine was shutted down successfully");
  }
}

void StateMachine::Reset(const Snapshot &snapshot) noexcept {
  assert(snapshot.assigned_ids_size() == snapshot.assigned_tasks_size());
  assert(snapshot.completed_ids_size() == snapshot.completed_tasks_size());

  std::lock_guard<std::mutex> guard(mutex_);

  assigned_.clear();
  completed_.clear();

  auto assigned_ids = snapshot.assigned_ids();
  auto assigned_tasks = snapshot.assigned_tasks();
  for (int i = 0; i < snapshot.assigned_ids_size(); ++i) {
    auto pair =
        std::make_pair(TaskIDWrapper(assigned_ids[i]), assigned_tasks[i]);
    assigned_.insert(pair);
  }

  auto completed_ids = snapshot.completed_ids();
  auto completed_tasks = snapshot.completed_tasks();
  for (int i = 0; i < snapshot.completed_ids_size(); ++i) {
    auto pair =
        std::make_pair(TaskIDWrapper(completed_ids[i]), completed_tasks[i]);
    completed_.insert(pair);
  }

  // snapshot will be created automatically in fork if needed
  // don't want to create snapshot here, because it may lock
  // state machine for a long time
  // snapshotted_index_ = 0;
  // snapshotted_term_ = 0;

  commit_index_ = snapshot.last_index();
  commit_term_ = snapshot.last_term();

  state_changed_.notify_all();
  LOG_DEBUG("%lu assigned and %lu completed tasks were restored from snapshot",
            assigned_.size(), completed_.size());
}

void StateMachine::snapshotThreadMain() noexcept {
  LOG_INFO("Node's snaphoting is enabled");
  Index snapshot_after = config_.GetSnapshotAfter();

  std::unique_lock<std::mutex> lock_guard(mutex_);
  while (!exiting_) {
    // cheking status of fork & log cleaning
    if ((commit_index_ - snapshotted_index_) >= snapshot_after &&
        !snapshotting) {
      snapshot();
    }

    state_changed_.wait(lock_guard);
  }
}

// we need to use copy-on-write (fork)
void StateMachine::snapshot() noexcept {
  LOG_INFO("Snapshotting...");
  auto failure = error(error::Code::FAILURE);
  snapshotting = true;
  std::thread(&StateMachine::forkedSnapshot, this, assigned_, completed_,
              commit_index_, commit_term_)
      .detach();
}

// Imitation of fork
void StateMachine::forkedSnapshot(HashTable assigned, HashTable completed,
                                  Index commit_index,
                                  Term commit_term) noexcept {
  Index idx = 0;
  std::string file_name = config_.GetSnapshotTo();
  std::ofstream output(file_name,
                       std::ios::out | std::ios::trunc | std::ios::binary);

  if (!output.is_open()) {
    LOG_ERROR("Unable to open file for snapshot writing");
    updateLog(idx, commit_term);
    return;
  }

  // snapshot filling
  Snapshot snapshot;
  snapshot.set_last_index(commit_index);
  snapshot.set_last_term(commit_term);

  for (auto pair : assigned) {
    auto id = snapshot.add_assigned_ids();
    id->CopyFrom(pair.first.id_);

    auto task = snapshot.add_assigned_tasks();
    task->CopyFrom(pair.second);
  }

  for (auto pair : completed) {
    auto id = snapshot.add_completed_ids();
    id->CopyFrom(pair.first.id_);

    auto task = snapshot.add_completed_tasks();
    task->CopyFrom(pair.second);
  }

  if (!snapshot.SerializeToOstream(&output)) {
    LOG_ERROR("Failed to write snapshot");
    updateLog(idx, commit_term);
    return;
  }

  output.close();
  idx = commit_index;
  updateLog(idx, commit_term);
}

// already locked
void StateMachine::updateLog(Index snapshot_idx, Term term) noexcept {
  std::lock_guard<std::mutex> lck(mutex_);
  if (snapshot_idx == 0) {
    LOG_DEBUG("Snapshot failed");
    snapshotting = false;
    return;
  }

  LOG_DEBUG("updateLog: dropbefore = %lu", snapshot_idx + 1);
  size_t count = log_.DropBefore(snapshot_idx + 1);
  snapshotted_index_ = snapshot_idx;
  snapshotted_term_ = term;
  snapshotting = false;
  LOG_INFO("Snapshotted successfully. Dropped %zu entries from log", count);
  state_changed_.notify_all();
}

// not initialzed if failure
StateMachine::SnapshotPtr StateMachine::GetSnapshot() const noexcept {
  SnapshotPtr snapshot = nullptr;
  auto snapshot_file = config_.GetSnapshotTo();
  std::ifstream input(snapshot_file, std::ios::in | std::ios::binary);
  if (!input.is_open()) {
    LOG_ERROR("Unable to fine file %s with snapshot", snapshot_file.c_str());
    return snapshot;
  } else {
    Snapshot temp_snapshot;
    if (!temp_snapshot.ParseFromIstream(&input)) {
      LOG_ERROR("Failed to parse snapshot file");
      return snapshot;
    }
    snapshot = std::make_unique<Snapshot>(std::move(temp_snapshot));
  }

  input.close();
  return snapshot;
}

StateMachine::Status StateMachine::SaveSnapshot(
    const Snapshot &snapshot) noexcept {
  auto failure = Status(Status::Code::FAILURE);
  std::string file_name = config_.GetSnapshotTo();
  std::ofstream output(file_name,
                       std::ios::out | std::ios::trunc | std::ios::binary);

  if (!output.is_open()) {
    LOG_ERROR("Unable to open file for snapshot writing");
    return failure;
  }

  if (!snapshot.SerializeToOstream(&output)) {
    LOG_ERROR("Failed to write snapshot");
    return failure;
  }

  std::lock_guard<std::mutex> guard(mutex_);
  Index last_index = snapshot.last_index();
  Term snapshot_term = snapshot.last_term();
  snapshotted_index_ = last_index;
  snapshotted_term_ = snapshot_term;
  snapshotting = false;

  return Status();
}

}  // namespace raft
}  // namespace abeille
