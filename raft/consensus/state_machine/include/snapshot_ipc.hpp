#ifndef ABEILLE_RAFT_LOG_SNAPSHOT_IPC_
#define ABEILLE_RAFT_LOG_SNAPSHOT_IPC_

#include <sys/msg.h>

#include <memory>

#include "common/errors/include/errors.hpp"
#include "common/utils/include/types.hpp"

#define MAX_SEND_SIZE 20

namespace abeille {
namespace raft {

class SnapshotIPC {
  struct Message {
    long type;
    char text[MAX_SEND_SIZE];
  };

  int msgqid;

 public:
  typedef error Status;

  SnapshotIPC();
  Status SendIndex(Index idx) const noexcept;
  // 0 if not in queue or error, 1 if error inside fork
  Index GetIndex() const noexcept;
};

}  // namespace raft
}  // namespace abeille

#endif  // ABEILLE_RAFT_LOG_SNAPSHOT_IPC_
