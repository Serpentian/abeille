#include "user/include/registry.hpp"

#include <google/protobuf/util/json_util.h>

#include <fstream>

#include "common/config/proto/config.pb.h"
#include "common/utils/include/convert.hpp"

using google::protobuf::util::JsonStringToMessage;
using google::protobuf::util::MessageToJsonString;

namespace abeille {
namespace user {

error Registry::Init(const std::string &filepath) noexcept {
  std::string data;
  error err = GetFileData(filepath, data);
  if (!err.ok()) {
    return err;
  }

  UserConfig user_config;
  auto status = JsonStringToMessage(data, &user_config);
  if (!status.ok()) {
    return error(status.message().as_string());
  }

  SetTaskResultsDirectory(user_config.task_results_dir());
  err = LoadSharedTaskLibrary(user_config.shared_lib_filepath());
  if (!err.ok()) {
    return err;
  }

  return error();
}

void Registry::SetTaskResultsDirectory(const std::string &dirpath) noexcept {
  task_results_dir = dirpath;
  std::filesystem::create_directory(dirpath);
}

error Registry::LoadSharedTaskLibrary(const std::string &filepath) noexcept {
  std::ifstream file(filepath);
  if (!file.good()) {
    return error("failed to open the lib file");
  }

  std::stringstream stream;
  stream << file.rdbuf();
  shared_task_library = std::move(stream.str());

  return error();
}

error Registry::SaveTaskResult(const std::string &filename,
                               const Bytes &task_result) const noexcept {
  auto filepath = task_results_dir / filename;
  std::ofstream file(filepath, std::ios::trunc);
  if (!file.good()) {
    return error("failed to open " + filepath.string());
  }

  Task::Result tr;
  tr.ParseFromString(task_result);

  std::string json;
  auto status = MessageToJsonString(tr, &json);
  if (!status.ok()) {
    return error(status.message().as_string());
  }

  file << json;

  return error();
}

}  // namespace user
}  // namespace abeille
