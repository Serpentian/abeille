#include "user/include/commands.hpp"

#include <google/protobuf/util/json_util.h>

#include <filesystem>
#include <fstream>
#include <sstream>

#include "common/errors/include/errors.hpp"
#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"
#include "user/include/registry.hpp"
#include "user/include/task.hpp"
#include "user/proto/task.pb.h"

namespace abeille {
namespace user {

CLIImpl::CLIImpl(std::shared_ptr<APIImpl> api) noexcept : api_(api) {
  handlers_helpers helpers = {{"ud <filepath>", "uploads data from <filepath> to the raft cluster"},
                              {"udd <dirpath>", "uploads data from <dirpath> to the raft cluster"}};
  SetHelpers(std::move(helpers));
}

std::string CLIImpl::CommandHandler(args_type args) noexcept {
  std::string command = args[0];
  if (command == "ud") {
    return UploadTaskData(args);
  } else if (command == "udd") {
    return UploadTaskDataDir(args);
  }

  return "command not found";
}

std::string CLIImpl::UploadTaskData(args_type args) noexcept {
  if (args.size() != 2) {
    return "invalid usage";
  }

  std::string filepath = args[1];
  error err = uploadTaskData(filepath);
  if (!err.ok()) {
    return err.what();
  }

  return "successfully uploaded task data";
}

std::string CLIImpl::UploadTaskDataDir(args_type args) noexcept {
  if (args.size() != 2) {
    return "invalid usage";
  }

  std::string dirpath = args[1];
  if (!std::filesystem::is_directory(dirpath)) {
    return "invalid path to a directory";
  }

  std::filesystem::directory_iterator dir(dirpath);
  for (const auto &file : dir) {
    if (!file.is_regular_file()) {
      continue;
    }

    error err = uploadTaskData(file.path());
    if (!err.ok()) {
      return err.what();
    }
  }

  return "successfully uploaded task data directory";
}

error CLIImpl::uploadTaskData(const std::string &filepath) noexcept {
  std::string task_data;
  error err = GetFileData(filepath, task_data);
  if (!err.ok()) {
    return err;
  }

  std::string filename = std::filesystem::path(filepath).filename();
  err = api_->UploadTaskData(filename, task_data);
  if (!err.ok()) {
    return err;
  }

  return error();
}

}  // namespace user
}  // namespace abeille
