#ifndef ABEILLE_USER_COMMANDS_HPP_
#define ABEILLE_USER_COMMANDS_HPP_

#include "common/utils/include/cli.hpp"
#include "user/include/api.hpp"
#include "user/include/user_client.hpp"

using abeille::common::CLI;
using abeille::user::Client;

#include <memory>

namespace abeille {
namespace user {

class CLIImpl : public CLI {
 public:
  explicit CLIImpl(std::shared_ptr<APIImpl> api_) noexcept;

  std::string CommandHandler(args_type args) noexcept override;

  std::string UploadTaskData(args_type args) noexcept;
  std::string UploadTaskDataDir(args_type args) noexcept;

 private:
  error uploadTaskData(const std::string &filepath) noexcept;

 private:
  std::shared_ptr<APIImpl> api_;
};

}  // namespace user
}  // namespace abeille

#endif  // ABEILLE_USER_COMMANDS_HPP_
