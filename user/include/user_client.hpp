#ifndef ABEILLE_USER_CLIENT_H_
#define ABEILLE_USER_CLIENT_H_

#include <grpcpp/grpcpp.h>

#include <condition_variable>
#include <memory>
#include <mutex>

#include "common/errors/include/errors.hpp"
#include "common/rpc/include/client.hpp"
#include "common/rpc/proto/abeille.grpc.pb.h"
#include "common/utils/include/types.hpp"
#include "user/include/registry.hpp"
#include "user/proto/task.pb.h"

using grpc::Channel;
using grpc::ClientContext;

namespace abeille {
namespace user {

using ConnReq = UserConnectRequest;
using ConnResp = UserConnectResponse;
using UserClient = abeille::rpc::Client<ConnReq, ConnResp, UserService>;

class Client : public UserClient {
 public:
  explicit Client(std::shared_ptr<Registry> registry) noexcept;

  void CommandHandler(const ConnResp &resp) override;
  void StatusHandler(ConnReq &req) override;

  error UploadData(const std::string &filename, const Bytes &task_data);

 private:
  error handleCommandRedirect(const ConnResp &resp);
  error handleCommandAssign(const ConnResp &resp);
  error handleCommandResult(const ConnResp &resp);

  error handleStatusUploadData(ConnReq &req);

 private:
  std::mutex mutex_;
  std::condition_variable cv_;

  uint64_t leader_id_ = 0;
  UserStatus status_ = USER_STATUS_IDLE;

  std::shared_ptr<Registry> registry_ = nullptr;
};

}  // namespace user
}  // namespace abeille

#endif  // ABEILLE_USER_CLIENT_H_
