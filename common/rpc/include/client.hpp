#ifndef ABEILLE_RPC_CLIENT_HPP_
#define ABEILLE_RPC_CLIENT_HPP_

#include <google/protobuf/util/json_util.h>
#include <grpcpp/grpcpp.h>

#include <atomic>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "common/config/proto/config.pb.h"
#include "common/errors/include/errors.hpp"
#include "common/utils/include/convert.hpp"
#include "common/utils/include/logger.hpp"

using google::protobuf::util::JsonStringToMessage;

using grpc::ClientContext;

namespace abeille {
namespace rpc {

template <typename ConnReq, typename ConnResp, typename Svc>
class Client {
 public:
  using SvcStub = typename Svc::Stub;
  using ConnStream = grpc::ClientReaderWriter<ConnReq, ConnResp>;

  Client() = default;
  ~Client() noexcept {
    Shutdown();
  }

  virtual error Init(const std::string &filepath) noexcept {
    std::string data;
    error err = GetFileData(filepath, data);
    if (!err.ok()) {
      return err;
    }

    ClientConfig client_config;
    auto status = JsonStringToMessage(data, &client_config);
    if (!status.ok()) {
      return error(status.message().as_string());
    }

    auto cluster_addresses = Repeated2Vec(client_config.cluster_addresses());
    err = SetClusterAddresses(cluster_addresses);
    if (!err.ok()) {
      return err;
    }

    return error();
  }

  error SetClusterAddresses(const std::vector<std::string> &cluster_addresses) noexcept {
    if (cluster_addresses.empty()) {
      return error("empty cluster addresses");
    }

    cluster_addresses_ = cluster_addresses;
    address_ = cluster_addresses_.front();

    return error();
  }

  virtual void CommandHandler(const ConnResp &resp) = 0;
  virtual void StatusHandler(ConnReq &req) = 0;

  void Run() {
    connect_thread_ = std::thread(&Client::connect, this);
  }

  void Shutdown() noexcept {
    if (!shutdown_) {
      LOG_INFO("shutting down...");
      shutdown_ = true;
      if (connect_stream_) {
        connect_stream_->WritesDone();
      }
      if (connect_thread_.joinable()) {
        connect_thread_.join();
      }
    }
  }

 protected:
  void reconnect(const std::string &address) {
    LOG_INFO("reconnecting to [%s]", address.c_str());
    address_ = address;
    connect_stream_->WritesDone();
  }

  void connect() {
    while (!shutdown_) {
      size_t index = 0;
      while (!handshake() && !shutdown_) {
        LOG_ERROR("failed to connect to [%s]", address_.c_str());

        address_ = cluster_addresses_[index];
        index = (index + 1) % cluster_addresses_.size();

        std::this_thread::sleep_for(std::chrono::seconds(1));
      }

      // if were shutdown, return to perform shutdown
      if (shutdown_) {
        return;
      }

      LOG_INFO("successfully connected to [%s]", address_.c_str());

      keepAlive();
    }
  }

  bool handshake() {
    auto channel = grpc::CreateChannel(address_, grpc::InsecureChannelCredentials());
    svc_stub_ = Svc::NewStub(channel);

    ctx_ = std::make_unique<ClientContext>();
    connect_stream_ = svc_stub_->Connect(ctx_.get());

    ConnReq req;
    bool ok = connect_stream_->Write(req);

    return ok;
  }

  void keepAlive() {
    ConnResp resp;
    while (connect_stream_->Read(&resp)) {
      CommandHandler(resp);

      ConnReq req;
      StatusHandler(req);

      connect_stream_->Write(req);
    }
  }

 protected:
  std::string address_;
  std::vector<std::string> cluster_addresses_;

  std::unique_ptr<ClientContext> ctx_ = nullptr;
  std::unique_ptr<SvcStub> svc_stub_ = nullptr;
  std::unique_ptr<ConnStream> connect_stream_ = nullptr;

  std::thread connect_thread_;
  std::atomic<bool> shutdown_ = false;
};

}  // namespace rpc
}  // namespace abeille

#endif  // ABEILLE_RPC_CLIENT_HPP_
