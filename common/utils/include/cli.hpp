#ifndef CLI_HPP_
#define CLI_HPP_

#include <exception>
#include <functional>
#include <iomanip>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "common/utils/include/convert.hpp"

namespace abeille {
namespace common {

static constexpr const char HELP_COMMAND[] = "help";
static constexpr const char EXIT_COMMAND[] = "exit";

class CLI {
 public:
  struct Helper {
    std::string command;
    std::string description;
  };

  using args_type = const std::vector<std::string> &;
  using handlers_helpers = std::vector<Helper>;

  void SetHelpers(handlers_helpers &&helpers) noexcept;

  bool Process(std::string &line);
  virtual std::string CommandHandler(args_type args) noexcept = 0;

  virtual void Help() const noexcept;
  virtual void Exit() noexcept;

 protected:
  bool exit_ = false;
  handlers_helpers helpers_;
  size_t max_command_size_ = 0;
};

}  // namespace common
}  // namespace abeille

#endif  // CLI_HPP_
