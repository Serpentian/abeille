#include "common/utils/include/convert.hpp"

#include <fstream>
#include <sstream>

static const uint64_t shift_mask[] = {40, 32, 24, 16};

uint64_t address2uint(const std::string &address) {
  size_t pos = address.find(':');
  uint64_t port = std::stoull(address.substr(pos + 1));
  std::string ip = address.substr(0, pos);

  uint64_t result = 0;
  auto splitted = Split(ip, '.');
  for (size_t i = 0; i < splitted.size(); ++i) {
    result |= std::stoull(splitted[i]) << shift_mask[i];
  }
  result |= port & 0xFFFF;

  return result;
}

std::string uint2address(uint64_t n) {
  return std::to_string(((n >> shift_mask[0]) & 0xFF)) + "." +
         std::to_string(((n >> shift_mask[1]) & 0xFF)) + "." +
         std::to_string(((n >> shift_mask[2]) & 0xFF)) + "." +
         std::to_string(((n >> shift_mask[3]) & 0xFF)) + ":" +
         std::to_string(n & 0xFFFF);
}

std::string ExtractAddress(const std::string &str) {
  return str.substr(str.find(':') + 1);
}

std::vector<std::string> Split(const std::string &str, char ch) {
  auto left = str.find_first_not_of(ch);
  auto right = str.find_first_of(ch, left);

  std::vector<std::string> splitted;
  while (right != std::string::npos) {
    splitted.push_back(str.substr(left, right - left));
    left = str.find_first_not_of(ch, right);
    right = str.find_first_of(ch, left);
  }

  // We arrive either at the last word or at a trailing delimiter.
  if (left != std::string::npos) {
    splitted.push_back(str.substr(left, str.size()));
  }

  return splitted;
}

error GetFileData(const std::string &filepath, std::string &data) noexcept {
  std::ifstream file(filepath);
  if (!file.good()) {
    return error("failed to open the file");
  }

  std::stringstream stream;
  stream << file.rdbuf();
  data = std::move(stream.str());

  return error();
}
